const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: {
		main: './src/index.js',
		vendor: [
			'axios', 'jquery', 'bootstrap', 'react-bootstrap'
		]
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				include: path.resolve(__dirname, 'src'),
				exclude: /node_modules/,
				use: ['babel-loader'],
			},
			{
				test: /\.css$/,
				use: [
					{ loader: 'style-loader' },
					{ loader: 'css-loader' }]
			},
			{
				type: 'javascript/auto',
				test: /\.json$/,
				use: ["json-loader"]
			},
			{
				test: /\.(jpe?g|png|gif|svg|woff(2)?|ttf|eot|svg|ico)$/,
				loader: 'file-loader',
				query: {
					name: 'assets/img/[name].[ext]'
				}
			},
		]
	},
	resolve: {
		modules: [path.resolve(__dirname, 'src'), 'node_modules'],
		extensions: ['*', '.js', '.jsx']
	},
	output: {
		path: path.resolve(__dirname + '/dist'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [
		new HtmlWebpackPlugin(
			{
				template: path.resolve(__dirname, 'public', 'index.html'),
				favicon: './src/assets/img/favicon.jpg',
				filename: './index.html'
			}
		),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.ProvidePlugin({
			jQuery: 'jquery',
			$: 'jquery',
			axios: "axios",
			bootstrap: 'bootstrap'
		})
	],
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		inline: true,
		compress: true,
		port: 3000,
		stats: "minimal",
		disableHostCheck: true,
		historyApiFallback: true,
		// hot: true,
		// clientLogLevel: 'none'
	}
}