import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import React from 'react';
import ReactDOM from 'react-dom';
import 'jquery'
import 'bootstrap'
import './index.css';
import './assets/img/favicon.jpg'
import App from './App';
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import thunk from 'redux-thunk'
import rootReducer from './redux/reducers/rootReducer'

const store = createStore(rootReducer, applyMiddleware(thunk))

ReactDOM.render(
	<Provider store={store}>
	<BrowserRouter>
		<App />
		</BrowserRouter>
	</Provider>, document.getElementById('root'));

module.hot.accept();