import React, { Component } from 'react';
import { withRouter, Route, Switch } from 'react-router-dom'

import NavBar from './components/layout/Navbar/NavBar';
import Register from './components/layout/Users/Register';
import ChangePassword from './components/layout/Users/ChangePassword'
import DeleteUser from './components/layout/Users/DeleteUser'
import Gallery from './components/gallery/Gallery';
import Login from './components/auth/Login';
import Footer from './components/layout/Footer';
import PrivateRoute from './components/common/PrivateRoute';
import Logout from './components/layout/Users/Logout';
import Weather from './components/widgets/Weather';

import { connect } from 'react-redux'
import { resetAction } from './redux/actions/usersActions'
import { insertImages } from './redux/actions/galleryActions'

class App extends Component {
  componentDidMount() {
    this.props.insertImages()
  }
  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.props.resetAction()
    }
  }
  render() {
    return (
        <div className="App">
          <NavBar />
          <Weather />
          <Switch>
            <Route exact path='/' component={Login} />
            <PrivateRoute path='/gallery' component={Gallery} />
            <PrivateRoute path='/user/register' component={Register} />
            <PrivateRoute path='/user/change' component={ChangePassword} />
            <PrivateRoute path='/user/delete' component={DeleteUser} />
            <PrivateRoute path='/logout' component={Logout} />
          </Switch>
          <Footer />
        </div>
     
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    resetAction: () => dispatch(resetAction()),
    insertImages: () => dispatch(insertImages())
  }
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
