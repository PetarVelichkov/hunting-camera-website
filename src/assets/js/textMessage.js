import React from 'react'

export const successText = (msg, hidden) => {
    let hiddenClass = (hidden) ? 'd-block mb-2' : 'd-none'
    return (<div id="success_message" className={hiddenClass} style={{
        fontSize: "15px",
        background: "#1bac30",
        textShadow: "0px 1px #000",
        color: "#FFF",
        padding: "5px",
        margin: "0 auto",
        borderRadius: "9px",
        textAlign: "center",
    }}>{msg}</div>)
}

export const incorrectText = (msg, hidden) => {
     let hiddenClass = (hidden) ? 'd-block mb-2' : 'd-none'
    return (<div id="error_message" className={hiddenClass} style={{
        fontSize: "15px",
        background: "rgb(255, 74, 74)",
        textShadow: "0px 1px #000",
        color: "#FFF",
        padding: "5px",
        margin: "0 auto",
        borderRadius: "9px",
        textAlign: "center",
    }}>{msg}</div>)
}