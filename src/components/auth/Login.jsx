import React, { Component } from 'react'
import avatar from '../../assets/img/front-picture.jpg'
import loading from '../../assets/img/ajax-loader-2.gif'
import { connect } from 'react-redux'
import { loginAction, setLoadingAction } from '../../redux/actions/authActions'
import Auth from './../common/auth';
import { Redirect } from 'react-router-dom'
import { incorrectText } from "../../assets/js/textMessage"

class Login extends Component {
	constructor(props) {
		super(props)
		this.state = {
			username: '',
			password: ''
		}
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault();
		this.props.setLoadingAction(true)
		this.props.loginAction(this.state)
		this.setState({
			username: '',
			password: ''
		})
	}
	render() {
		if (Auth.isUserLogin()) return <Redirect to='/gallery' />
		let hidden = (this.props.auth.error) ? true : false
		let loader = (this.props.auth.loading) ? loading : ''
		return (
			<div className="container mt-2 mb-2">
				<div className="card m-auto p-0 bg-light col-sm-10 col-md-6 col-lg-6">
					<img className="card-img-top" src={avatar} alt="Card image cap" />
					<div className="card-body">
						<form onSubmit={this.handleSubmit}>
							<h5 className="card-header mb-3 text-muted">Ловна камера "Начова"</h5>
							{incorrectText(this.props.auth.error_msg, hidden)}
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-user fa-lg" aria-hidden="true" /></span>
								<input type="text" onChange={this.handleChange} className="form-control" id="username" placeholder="Потребителско име" value={this.state.username} required />
							</div>
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-lock fa-lg" aria-hidden="true" /></span>
								<input type="password" onChange={this.handleChange} className="form-control" id="password" placeholder="Парола" value={this.state.password} required />
							</div>
							<div className="form-group">
								<input type="submit" className="btn btn-info" id="submit" value="Влез" />
							</div>
							<span className="bg-light">
								<img src={loader} className="img-responsive" />
							</span>
						</form>
					</div>
				</div>
			</div >
		)
	}
}

// REDUX
const mapStateToProps = (state) => {
	return {
		auth: state.auth
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		loginAction: (creds) => dispatch(loginAction(creds)),
		setLoadingAction: (value) => dispatch(setLoadingAction(value))
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);