import React, { Component } from 'react'

export default class Clock extends Component {
	constructor(props) {
		super(props)
		this.state = {
			time: new Date()
		}
	}
	componentDidMount() {
		this.interval = setInterval(this.update, 1000)
	}
	componentWillUnmount() {
		clearInterval(this.interval)
	}
	update = () => {
		this.setState({
			time: new Date()
		})
	}
  render() {
	return (
	  <span>
		{this.state.time.toLocaleTimeString()}
	  </span>
	)
  }
}
