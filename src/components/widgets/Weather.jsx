import React, { Component } from 'react'
import { getSymbolKey } from "../../assets/js/symbols"
import '../../assets/style/Weather.css'
import Clock from "./Clock"
import axios from 'axios'
const XMLParser = require('react-xml-parser');

class Weather extends Component {
	isMounted = false;
	constructor(props) {
		super(props);
		this.state = {
			temperature: ''
		}
	}

	componentDidMount() {
		this.isMounted = true
		this.getWeatherData()
	}
	componentWillUnmount() {
		this.isMounted = false
	}
	//https://api.met.no/weatherapi/locationforecast/1.9/?lat=41.684554&lon=24.797593&msl=1500
	//https://api.openweathermap.org/data/2.5/weather?q=727030&units=metric&appid=c9aa1465505695dfa1aa725d343f7904

	getWeatherData = () => {
		const LOCATION = {
			lat: 41.684554,
			lon: 24.797593,
			msl: 1400
		};
		return axios.get('https://api.met.no/weatherapi/locationforecast/1.9/?lat=41.684554&lon=24.797593&msl=1400')
			.then((response) => {
				let data = new XMLParser().parseFromString(response.data)
				let temperature = data.getElementsByTagName('temperature')[0].attributes.value
				//let symbol = data.getElementsByTagName('symbol')[0].attributes.id
				if (this.isMounted) {
					this.setState({
						temperature: temperature
					})
				}


				/*return axios.get(`https://api.met.no/weatherapi/weathericon/1.1/?symbol=${symbol}&content_type=image/png`)
				.then((data) => {
					this.setState({
						temperature: temperature,
						symbol: data.data.url
					})
				})
				.catch((error) => {
					console.log('error in get symbols', error)
				})*/
			})
			.catch((error) => {
				console.log('error in get temperature', error)
			})
	}

	render() {
		return (
			<div className="col-lg-6 col-md-8 col-sm-12 flex-container center weather">
				<span style={{ paddingLeft: 0 }}><strong>{this.state.temperature}°C</strong></span>
				{/* <img alt='' src={this.state.symbol}/> */}
				<span>Н.В.:<strong>1510 м.</strong></span>
				<span>Час:<strong>{<Clock />}</strong></span>
			</div>
		)
	}
}

export default Weather;