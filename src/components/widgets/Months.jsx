import React from 'react'

const Months = ({setMonth}) => {
	let handleClick = (e) => {
		setMonth(parseInt(e.target.dataset.id))
	}
	return (
		<div className="container mt-1">
			<div className="btn-group">
				<a className="btn btn-light btn-sm dropdown-toggle" data-toggle="dropdown" href="#">Избери месец <span className="caret"></span></a>
				<ul className="dropdown-menu">
					<li className="dropdown-item"><a href="#" data-id="1" onClick={handleClick}>Януари</a></li>
					<li className="dropdown-item"><a href="#" data-id="2" onClick={handleClick}>Февруари</a></li>
					<li className="dropdown-item"><a href="#" data-id="3" onClick={handleClick}>Март</a></li>
					<li className="dropdown-item"><a href="#" data-id="4" onClick={handleClick}>Април</a></li>
					<li className="dropdown-item"><a href="#" data-id="5" onClick={handleClick}>Май</a></li>
					<li className="dropdown-item"><a href="#" data-id="6" onClick={handleClick}>Юни</a></li>
					<li className="dropdown-item"><a href="#" data-id="7" onClick={handleClick}>Юли</a></li>
					<li className="dropdown-item"><a href="#" data-id="8" onClick={handleClick}>Август</a></li>
					<li className="dropdown-item"><a href="#" data-id="9" onClick={handleClick}>Септември</a></li>
					<li className="dropdown-item"><a href="#" data-id="10" onClick={handleClick}>Октомври</a></li>
					<li className="dropdown-item"><a href="#" data-id="11" onClick={handleClick}>Ноември</a></li>
					<li className="dropdown-item"><a href="#" data-id="12" onClick={handleClick}>Декември</a></li>
					
				</ul>
			</div>
		</div>

	)
}

export default Months
