import React, { Component } from 'react'
import { connect } from 'react-redux';
import {
	getImagesAction,
	setMonthAction,
	setItemsOnPageAction,
	setImageForDelete,
	removeImageForDelete,
	deleteImagesAction,
	insertImages
} from '../../redux/actions/galleryActions'
import Months from './../widgets/Months';
import LightboxRender from './LightboxRender';
import ItemsOnPage from './ItemsOnPage';

class Gallery extends Component {
	constructor(props) {
		super(props)
	}
	componentDidMount() {
		this.props.getImagesAction(this.props.gallery.filters.fromDate, this.props.gallery.filters.toDate)
		//this.props.insertImages()
	}
	componentWillReceiveProps(nextProps) {
		if (nextProps.gallery.filters !== this.props.gallery.filters) {
			nextProps.getImagesAction(nextProps.gallery.filters.fromDate, nextProps.gallery.filters.toDate)
		}
	}

	render() {
		return (
			<div>
				<Months setMonth={this.props.setMonthAction} />
				{/* <ItemsOnPage setItemsOnPage={this.props.setItemsOnPageAction} /> */}
				<LightboxRender />
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		gallery: state.gallery
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		getImagesAction: (fromDate, toDate) => dispatch(getImagesAction(fromDate, toDate)),
		setMonthAction: (month) => dispatch(setMonthAction(month)),
		setItemsOnPageAction: (value) => dispatch(setItemsOnPageAction(value)),
		setImageForDelete: (image) => dispatch(setImageForDelete(image)),
		removeImageForDelete: (image) => dispatch(removeImageForDelete(image)),
		deleteImagesAction: (images) => dispatch(deleteImagesAction(images)),
		insertImages: () => dispatch(insertImages())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Gallery)
