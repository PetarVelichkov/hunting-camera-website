import React from 'react'

const DeleteImage = ({ deleteImages, clearDeleteImages }) => {
	const handleDelete = () => {
		if (confirm('Изтриване на избраните снимки')) {
			deleteImages()
			$("input[type=checkbox]").each(function () {
				this.checked = false
			})
			clearDeleteImages()
		}
	}
	return (
		<div className="mb-1">
			<button className="btn btn-danger" onClick={handleDelete}>Изтрий снимки</button>
		</div>
	)
}

export default DeleteImage
