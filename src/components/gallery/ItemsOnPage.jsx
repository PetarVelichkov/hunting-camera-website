import React from 'react';

const ItemsOnPage = ({ setItemsOnPage }) => {
	const onChangeHandle = (e) => {
		setItemsOnPage(parseInt(e.target.value))
	}
	return (
		<div className="container mt-1 col-2">
			<select className="custom-select" onChange={onChangeHandle}>
				<option defaultValue=''>Покажи...</option>
				<option value="12">12</option>
				<option value="16">16</option>
				<option value="20">20</option>
				<option value="24">24</option>
				<option value="28">28</option>
			</select>
		</div>
	)
}

export default ItemsOnPage