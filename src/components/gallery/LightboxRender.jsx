import React, { Component } from 'react'
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'
import JwPagination from 'jw-react-pagination'
import Auth from '../common/auth'
import DeleteImage from './DeleteImage';
import { connect } from 'react-redux';
import {
	setImageForDelete,
	removeImageForDelete,
	deleteImagesAction,
	getImagesAction,
	clearDeleteImages
} from '../../redux/actions/galleryActions'

class LightboxRender extends Component {
	constructor(props) {
		super(props)
		this.state = {
			photoIndex: 0,
			isOpen: false,
			pageOfItems: [],
		}
	}
	changePaginationClass = () => {
		//Chane pagination class and text
		setTimeout(() => {
			$(".pagination li").addClass("page-item");
			$(".pagination li a").addClass("page-link");
			$(".pagination li.first a").text("Първа");
			$("ul.pagination li.last a").text("Последна");
			$("ul.pagination li.previous a").text("<");
			$("ul.pagination li.next a").text(">");
		}, 10)
	}
	onChangePage = (pageOfItems) => {
		// update local state with new page of items
		this.setState({ pageOfItems });
	}
	onClickThumb = (key, e) => {
		this.setState({
			isOpen: true,
			photoIndex: key
		})
	};
	handleCheckBox = (e) => {
		//console.log(e.target.dataset)
		let id = e.target.dataset.id
		let image = e.target.dataset.image
		if (e.target.checked) {
			this.props.setImageForDelete({ id: id, image: image })
		} else {
			this.props.removeImageForDelete({ id: id, image: image })
		}
	}
	renderImage = (key, id, src) => {
		return (
			<div className="col-6 col-sm-6 col-md-4 col-lg-3 d-inline-block mb-2" key={key}>
				{Auth.isUserLogin() === 1 ? (//data-image-src={img} //data-image-src={img} for img tag
					<input type="checkbox" onChange={this.handleCheckBox} data-image={src} data-id={id} defaultChecked={false} className="checkbox position-absolute" />) : null}
				<a type="button" onClick={this.onClickThumb.bind(this, key)}>
					<img className="thumb-img img-thumbnail" src={src} />
				</a>
			</div>
		);
	};
	render() {
		const { isOpen, pageOfItems, photoIndex } = this.state
		const { images, imagesForDelete, items } = this.props.gallery
		const { fromDate, toDate } = this.props.gallery.filters
		let deleteImages = (imagesForDelete.length > 0) ? 
		<DeleteImage
		  deleteImages={() => this.props.deleteImagesAction(imagesForDelete, fromDate, toDate)}
		  clearDeleteImages={this.props.clearDeleteImages} /> : null
		
		return (
			<div className='mt-2'>
				{this.changePaginationClass()}
				{deleteImages}
				<div className="container">
					{pageOfItems.map((data, key) => this.renderImage(key, data.id, data.image))}
				</div>
				<div className="container mt-1">
					<JwPagination items={images} onChangePage={this.onChangePage} pageSize={items} />
				</div>
				{/* imagePadding={35} ако е нужно */}
				{isOpen && (
					<Lightbox
						mainSrc={pageOfItems[photoIndex].image}
						nextSrc={pageOfItems[(photoIndex + 1) % pageOfItems.length].image}
						prevSrc={pageOfItems[(photoIndex + pageOfItems.length - 1) % pageOfItems.length].image}
						onCloseRequest={() => this.setState({ isOpen: false })}
						onMovePrevRequest={() =>
							this.setState({
								photoIndex: (photoIndex + pageOfItems.length - 1) % pageOfItems.length
							})}
						onMoveNextRequest={() =>
							this.setState({
								photoIndex: (photoIndex + 1) % pageOfItems.length
							})}
					/>
				)}
			</div>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		gallery: state.gallery
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		setImageForDelete: (image) => dispatch(setImageForDelete(image)),
		removeImageForDelete: (image) => dispatch(removeImageForDelete(image)),
		deleteImagesAction: (images, from, to) => dispatch(deleteImagesAction(images, from, to)),
		getImagesAction: (fromDate, toDate) => dispatch(getImagesAction(fromDate, toDate)),
		clearDeleteImages: () => dispatch(clearDeleteImages())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(LightboxRender)