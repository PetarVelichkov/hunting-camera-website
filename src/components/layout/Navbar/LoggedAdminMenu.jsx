import React from 'react'
import { NavLink } from 'react-router-dom'

const LoggedAdminMenu = () => {
	return (
		<div className="collapse navbar-collapse justify-content-end" id="navbarToggler">
			<ul className="navbar-nav mx-auto mt-2 mt-lg-0">
				<li className="nav-item">
					<NavLink to='/user/register' className="nav-link text-light">Регистрация</NavLink>
				</li>
				<li className="nav-item">
					<NavLink to='/user/change' className="nav-link text-light">Смяна на парола</NavLink>
				</li>
				<li className="nav-item">
					<NavLink to='/user/delete' className="nav-link text-light">Изтриване</NavLink>
				</li>
				<li className="nav-item">
					<NavLink to='/logout' className="nav-link text-light">Изход</NavLink>
				</li>
			</ul>
		</div>
	)
}

export default LoggedAdminMenu
