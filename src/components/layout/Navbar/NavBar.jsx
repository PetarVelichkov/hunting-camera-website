import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import LoggedAdminMenu from './LoggedAdminMenu';
import LoggedUserMenu from './LoggedUserMenu'
import NotLogin from './NotLogin';
import { connect } from 'react-redux'
import Auth from '../../common/auth'
import { withRouter } from 'react-router-dom'

class NavBar extends Component {
	state = {
		expand: true
	}
	handleClick = () => {
		if (this.state.expand) {
			$("#navbarToggler").show()
		} else {
			$("#navbarToggler").hide()
		}
		this.setState({
			expand: !this.state.expand
		})
	}
	render() {
		let adminLinks = (Auth.isUserLogin() === 1 || this.props.auth.user.role_id === '1') ? <LoggedAdminMenu /> : null
		let userLinks = (Auth.isUserLogin() === 2 || this.props.auth.user.role_id === '2') ? <LoggedUserMenu /> : null
		let notLogin = (!Auth.isUserLogin()) ? <NotLogin /> : null
		let brand = (Auth.GetUser() || this.props.auth.user.username) ? <Link className="navbar-brand text-light" to='/gallery'>Ловна камера</Link> : null;
		return (
			<nav className="navbar navbar-expand-lg navbar-expand-md navbar-light bg-info">
				<div className="container">
					{brand}

					<button className="navbar-toggler text-light" onClick={this.handleClick} type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded={this.state.expand} aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
					</button>

					{adminLinks}
					{userLinks}
					{notLogin}

				</div>
			</nav>
		)
	}
}

const mapStateToProps = (state) => {
	return {
		auth: state.auth
	}
}

export default withRouter(connect(mapStateToProps)(NavBar))
