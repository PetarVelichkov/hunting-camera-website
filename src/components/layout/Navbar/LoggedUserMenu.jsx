import React from 'react'
import { NavLink } from 'react-router-dom'

const LoggedUserMenu = () => {
	return (
		<div className="collapse navbar-collapse justify-content-end" id="navbarToggler">
			<ul className="navbar-nav mt-2 mt-lg- ">
				<li className="nav-item">
					<NavLink to='/logout' className="nav-link text-light">Изход</NavLink>
				</li>
			</ul>
		</div>
	)
}

export default LoggedUserMenu
