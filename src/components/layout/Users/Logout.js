import React from 'react'
import { connect } from 'react-redux'
import { logoutAction } from '../../../redux/actions/authActions'
import { Redirect } from 'react-router-dom'

const Logout = (props) => {
	props.logoutAction()
	return <Redirect to='/' />
}

const mapDispatchToProps = (dispatch) => {
	return {
		logoutAction: () => dispatch(logoutAction())
	}
}
export default connect(null, mapDispatchToProps)(Logout)
