import React, { Component } from 'react'
import { connect } from 'react-redux'
import loading from 'assets/img/ajax-loader-2.gif'
import { registerAction, setLoadingAction, resetAction } from '../../../redux/actions/usersActions'
import { incorrectText, successText } from '../../../assets/js/textMessage'

class Register extends Component {
	state = {
		username: '',
		password: '',
		confirm: '',
		role_id: '2'
	}
	componentDidMount() {
		$('#error_message, #success_message').on('click', () => {
			this.props.resetAction()
			$('#error_message').hide()
			$('#success_message').hide()
		})
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault()
		this.props.setLoadingAction(true)
		this.props.registerAction(this.state)
		this.setState({
			username: '',
			password: '',
			confirm: '',
			role_id: '2'
		})
	}

	render() {
		let hiddenIncorrect = (this.props.users.userAction.error) ? true : false
		let hiddenSucces = (this.props.users.userAction.msg) ? true : false
		let loader = (this.props.users.loading) ? loading : ''

		return (
			<div className="container mt-2 mb-2">
				<div className="card m-auto p-0 bg-light col-sm-10 col-md-6 col-lg-6">
					<div className="card-body">
						<form onSubmit={this.handleSubmit}>
							<h5 className="card-header mb-3 text-muted">Регистрация</h5>
							{incorrectText(this.props.users.userAction.error_msg, hiddenIncorrect)}
							{successText(this.props.users.userAction.msg, hiddenSucces)}
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-user fa-lg" aria-hidden="true" /></span>
								<input type="text" onChange={this.handleChange} className="form-control" id="username" placeholder="Потребителско име" required />
							</div>
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-lock fa-lg" aria-hidden="true" /></span>
								<input type="password" onChange={this.handleChange} className="form-control" id="password" placeholder="Парола" required />
							</div>
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-lock fa-lg" aria-hidden="true" /></span>
								<input type="password" onChange={this.handleChange} className="form-control" id="confirm" placeholder="Потвърди паролата" required />
							</div>
							<div className="form-group input-group">
								<select name="roles" id="role_id" className="custom-select" onChange={this.handleChange}>
									<option value="2">User</option>
									<option value="1">Admin</option>
								</select>
							</div>
							<div className="form-group">
								<input type="submit" className="btn btn-info" id="submit" value="Регистрация" />
							</div>
							<span className="bg-light">
								<img src={loader} className="img-responsive" />
							</span>
						</form>
					</div>
				</div>
			</div >
		)
	}
}

const mapStateToProps = (state) => {
	return {
		users: state.users
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		registerAction: (creds) => dispatch(registerAction(creds)),
		setLoadingAction: (value) => dispatch(setLoadingAction(value)),
		resetAction: () => dispatch(resetAction())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)
