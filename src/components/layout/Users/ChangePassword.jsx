import React, { Component } from 'react'
import { connect } from 'react-redux'
import { changePasswordAction, getAllUserAction } from '../../../redux/actions/usersActions'
import { incorrectText, successText } from '../../../assets/js/textMessage'

class ChangePassword extends Component {
	state = {
		userId: '',
		password: ''
	}
	componentDidMount() {
		this.props.getAllUserAction()
	}
	handleChange = (e) => {
		this.setState({
			[e.target.id]: e.target.value
		})
	}
	handleSubmit = (e) => {
		e.preventDefault()
		this.props.changePasswordAction(this.state)
	}
	render() {
		let hiddenIncorrect = (this.props.users.userAction.error) ? true : false
		let hiddenSucces = (this.props.users.userAction.msg) ? true : false
		let users = (this.props.users.users.data) ? this.props.users.users.data : []

		return (
			<div className="container mt-2 mb-2">
				<div className="card m-auto p-0 bg-light col-sm-10 col-md-6 col-lg-6">
					<div className="card-body">
						<form onSubmit={this.handleSubmit}>
							<h5 className="card-header mb-3 text-muted">Смяна на парола</h5>
							{incorrectText(this.props.users.userAction.error_msg, hiddenIncorrect)}
							{successText(this.props.users.userAction.msg, hiddenSucces)}
							<div className="form-group input-group">
								<select name="roles" id="userId" className="custom-select" onChange={this.handleChange} required >
								<option defaultValue=''>Избери потребител...</option>
									{users.map((user, i) => {
										return <option key={i} value={user.id}>{user.username}</option>
									})}
								</select>
							</div>
							<div className="form-group input-group">
								<span className="input-group-text"><i className="fa fa-lock fa-lg" aria-hidden="true" /></span>
								<input type="password" onChange={this.handleChange} className="form-control" id="password" placeholder="Парола" required />
							</div>
							<div className="form-group">
								<input type="submit" className="btn btn-info" id="submit" value="Смяна на парола" />
							</div>
						</form>
					</div>
				</div>
			</div >
		)
	}
}

const mapStateToProps = (state) => {
	return {
		users: state.users
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		changePasswordAction: (creds) => dispatch(changePasswordAction(creds)),
		getAllUserAction: () => dispatch(getAllUserAction())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)

