import React from 'react'

const Footer = () => {
	return (
		<div className="container fixed-bottom">
			<div className="row">
				<div className="col-xs-12 col-sm-12 col-md-12 mt-2 mt-sm-2 pt-1 text-center text-light bg-info">
					<p className="h6"><i className="fa fa-copyright" />Всички права запазени.<span className="ml-2">
					<strong>2018-2019 Петър Величков</strong></span></p>
				</div>
			</div>
		</div>
	)
}

export default Footer
