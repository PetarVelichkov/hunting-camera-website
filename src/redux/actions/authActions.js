import axios from 'axios';
import { config } from '../../config/config'
const url = config.api_host;

export const loginAction = ({ username, password }) => {
	return (dispatch, getState) => {
		axios.post(url + "?", {
			apiCall: 'login',
			username: encodeURI(username),
			password: encodeURI(password),
		})
			.then((response) => {
				if (!response.data.error) {
					dispatch({
						type: "LOGIN",
						payload: response.data
					})
				} else {
					dispatch({
						type: "LOGIN_ERROR",
						payload: response.data
					})
				}
			})
	}
}

export const setLoadingAction = (value) => {
	return (dispatch) => {
		dispatch({
			type: "LOADING",
			payload: value
		})
	}
}

export const logoutAction = () => {
	return (dispatch) => {
		dispatch({
			type: "LOGOUT"
		})
	}
}