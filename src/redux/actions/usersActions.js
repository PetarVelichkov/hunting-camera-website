import axios from 'axios';
import { config } from '../../config/config'
const url = config.api_host;

export const registerAction = ({username, password, confirm, role_id}) => {
	return (dispatch) => {
		axios.post(url + "?", {
			apiCall: 'register',
			username: username,
			password: password,
			confirm: confirm,
			role_id: role_id
		})
		.then((response) => {
			if (!response.data.error) {
				dispatch({
					type: 'REGISTER',
					payload: response.data
				})
			} else {
				dispatch({
					type: 'REGISTER_ERROR',
					payload: response.data
				})
			}
		})
	}
}

export const getAllUserAction = () => {
	return (dispatch) => {
		axios.post(url + '?', {
			apiCall: 'getAllUsers'
		})
		.then((response) => {
			dispatch({
				type: 'ALL_USERS',
				payload: response.data
			})
		})
		.catch((error) => {
			dispatch({
				type: 'ERROR_USERS',
				payload: error.data
			})
		})
	}
}

export const changePasswordAction = ({userId, password}) => {
	return (dispatch) => {
		axios.post(url + '?', {
			apiCall: 'changePassword',
			userId: userId,
			password: password
		})
		.then((response) => {
			if (!response.data.error) {
				dispatch({
					type: 'CHANGE_PASSWORD',
					payload: response.data
				})
			} else {
				dispatch({
					type: 'ERROR_CHANGE_PASSWORD',
					payload: response.data
				})
			}
		})
	}
}


export const deleteUserAction = ({userId}) => {
	return (dispatch) => {
		axios.post(url + '?', {
			apiCall: 'deleteUser',
			userId: userId
		})
		.then((response) => {
			if (!response.data.error) {
				dispatch({
					type: 'DELETE_USER',
					payload: response.data
				})
			} else {
				dispatch({
					type: 'ERROR_DELETE_USER',
					payload: response.data
				})
			}
		})
	}
}
export const setLoadingAction = (value) => {
	return (dispatch) => {
		dispatch({
			type: "LOADING",
			payload: value
		})
	}
}
export const resetAction = () => {
	return (dispatch) => {
		dispatch({
			type: 'RESET',
			payload: true
		})
	}
}