import axios from 'axios';
import { config } from '../../config/config'
const url = config.api_host;

export const getImagesAction = (from, to) => {
	return (dispatch, getState) => {
		axios.post(url + "?", {
			apiCall: 'getImages',
			from: from,
			to: to
		})
		.then((response) => {
			dispatch({
				type: "GET_IMAGES",
				payload: response.data
			})
		})
		.catch((err) => {
			dispatch({
				type: "GET_IMAGES_ERROR",
				payload: err
			})
		})
	}
}
export const deleteImagesAction = (images, from, to) => {
	return (dispatch) => {
		axios.post(url + '?', {
			apiCall: 'deleteImages',
			images: images,
			from: from,
			to: to
		})
		.then((response) => {
			dispatch({
				type: 'DELETE_IMAGES',
				payload: response.data
			})
		})
		
	}
}
export const setMonthAction = (month) => {
	return (dispatch) => {
		dispatch({
			type: 'SET_MONTH',
			payload: month
		})
	}
}
export const setItemsOnPageAction = (value) => {
	return (dispatch) => {
		dispatch({
			type: 'SET_ITEMS',
			payload: value
		})
	}
}
export const setImageForDelete = (image) => {
	return (dispatch) => {
		dispatch({
			type: 'SET_DELETE_IMAGE',
			payload: image
		})
	}
}
export const removeImageForDelete = (image) => {
	return (dispatch) => {
		dispatch({
			type: 'REMOVE_DELETE_IMAGE',
			payload: image
		})
	}
}
export const clearDeleteImages = () => {
	return (dispatch) => {
		dispatch({
			type: 'CLEAR_DELETE_IMAGES',
			payload: true
		})
	}
}
export const insertImages = () => {
	return (dispatch) => {
		axios.post(url + '?', {
			apiCall: 'insert'
		})
	}
}