const initState = {
	loading: false,
	userAction: {
		error: '',
		msg: '',
		error_msg: ''
	},
	users: {
		data: []
	},

}

const usersReducer = (state = initState, action) => {
	switch (action.type) {
		case 'RESET':
			return { ...initState }
		case 'REGISTER':
		case 'REGISTER_ERROR':
		case 'CHANGE_PASSWORD':
		case 'ERROR_CHANGE_PASSWORD':
		case 'DELETE_USER':
		case 'ERROR_DELETE_USER':
			return {
				...state,
				loading: false,
				userAction: {
					...action.payload
				}
			}
		case 'ALL_USERS':
			return {
				...state,
				users: {
					data: action.payload
				}
			}
		case 'LOADING':
			return {
				...state,
				loading: action.payload
			}
		case 'ERROR_USERS':
			return {
				...state,
				users: {
					data: action.payload
				}
			}

		default:
			return state
	}
}

export default usersReducer