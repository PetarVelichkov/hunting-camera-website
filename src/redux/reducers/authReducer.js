import Auth from "../../components/common/auth"
import { setLoadingAction } from '../actions/authActions'

const initState = {
	loading: false,
	error: false,
	error_msg: '',
	user: {
		uid: '',
		role_id: '',
		username: ''
	}
}

const authReducer = (state = initState, action) => {
	switch (action.type) {
		case 'LOGIN':
			Auth.UserLogin(action.payload.user)
			return {
				...state,
				loading: false,
				error_msg: '',
				...action.payload
			}
		case 'LOGIN_ERROR':
			return {
				...state,
				error: action.payload.error,
				error_msg: action.payload.error_msg
			}
		case 'LOADING':
			return {
				...state,
				loading: action.payload
			}
		case 'LOGOUT':
			Auth.RemoveUser()
			return initState
		default:
			return state
	}
}

export default authReducer