import { combineReducers } from 'redux'
import authReducer from './authReducer'
import galleryReducer from './galleryReducer'
import usersReducer from './usersReducer'


const rootReducer = combineReducers({
	gallery: galleryReducer,
	auth: authReducer,
	users: usersReducer
})

export default rootReducer