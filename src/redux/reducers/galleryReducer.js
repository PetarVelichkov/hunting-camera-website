let date = new Date();
let firstDate = new Date(date.getFullYear(), date.getMonth());
let lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

let fromDate = firstDate.getFullYear() + "-" + ("0" + (firstDate.getMonth() + 1)).slice(-2) + "-" + ("0" + firstDate.getDate()).slice(-2);
let toDate = lastDate.getFullYear() + "-" + ("0" + (lastDate.getMonth() + 1)).slice(-2) + "-" + ("0" + lastDate.getDate()).slice(-2);

const initState = {
	images: [],
	imagesForDelete: [],
	items: 12,
	filters: {
		fromDate: fromDate,
		toDate: toDate
	}
}

const galleryReducer = (state = initState, action) => {
	switch (action.type) {
		case 'GET_IMAGES':
		case "DELETE_IMAGES":
			return {
				...state,
				images: action.payload
			}
		case 'SET_MONTH':
			let date = new Date();
			let firstDate = new Date(date.getFullYear(), action.payload - 1);
			let lastDate = new Date(date.getFullYear(), action.payload, 0);

			let fromDate = firstDate.getFullYear() + "-" + ("0" + (firstDate.getMonth() + 1)).slice(-2) + "-" + ("0" + firstDate.getDate()).slice(-2);
			let toDate = lastDate.getFullYear() + "-" + ("0" + (lastDate.getMonth() + 1)).slice(-2) + "-" + ("0" + lastDate.getDate()).slice(-2);
			return {
				...state,
				filters: {
					fromDate: fromDate,
					toDate: toDate
				}
			}
		case "SET_ITEMS":
			return {
				...state,
				items: action.payload
			}
		case "SET_DELETE_IMAGE":
			return {
				...state,
				imagesForDelete: [...state.imagesForDelete, action.payload]
			}
		case "REMOVE_DELETE_IMAGE":
			let deleteImages = state.imagesForDelete.filter(image => {
				return image.id !== action.payload.id
			})
			return {
				...state,
				imagesForDelete: deleteImages
			}
		case "CLEAR_DELETE_IMAGES":
			return {
				...state,
				imagesForDelete: []
			}
		default:
			return state
	}
}

export default galleryReducer