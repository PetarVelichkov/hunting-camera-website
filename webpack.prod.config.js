const webpack = require('webpack')
const merge = require('webpack-merge')

const devConfig = require('./webpack.dev.config');
const optimizationConfig = require('./webpack.opt.config');

const productionConfiguration = function (env) {
	const NODE_ENV = env.NODE_ENV ? env.NODE_ENV : 'development';
	return {
	  plugins: [
		new webpack.DefinePlugin({ 'process.env.NODE_ENV': JSON.stringify(NODE_ENV) }),
	  ]
	};
  }
  
  module.exports = merge.smart(devConfig, optimizationConfig, productionConfiguration);